package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenTapsCrmPage extends ViewLeadPage {
	
	public OpenTapsCrmPage() {
		
		PageFactory.initElements(driver, this);
			
	}
	
	@CacheLookup
	@FindBy(id ="updateLeadForm_companyName")
	WebElement eleCompanyName;
	
	public OpenTapsCrmPage typeCompanyName(String cName) {
		//WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		eleCompanyName.clear();
		type(eleCompanyName, cName);
		return this;
	}
	
	
	
	public ViewLeadPage clickUpdateCname() {
		WebElement eleUpdateclick = locateElement("xpath", "//input[@value='Update']");
		click( eleUpdateclick);
		return new ViewLeadPage();
	}

	
	

}
