package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testCaseDescription ="Delete a lead";
		category = "SIT";
		author= "Manjula";
		dataSheetName="deleteTC003";
}
@Test(dataProvider="fetchData")
public  void deleteLead(String PhoneNumber ) throws InterruptedException   {
	new MyHomePage()
	.clickLeads()
	.clickFindLeads()
	.clickPhone()
	.typePhoneNumber(PhoneNumber)
	.clickFindLeadsbutton()
	.clickFirstRow()
	.clickDelete()
	.clickFindLeads()
	.typeLeadid()
	.clickFindLeadsbutton();
	

}
}
