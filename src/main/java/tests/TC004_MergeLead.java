package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
	

	@BeforeClass
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testCaseDescription ="Merge two leads";
		category = "SIT";
		author= "Manjula";
		
}
@Test
public  void mergeLead() throws InterruptedException   {
	new MyHomePage()
	.clickLeads()
	.clickMergeLeads()
	.clickFromLead()
	.clickRowOne()
	.clickToLead()
	.clickRowTwo()
	.clickMergeButton()
	.clickFindLeads();
	
	

}

}
